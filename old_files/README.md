# Prolog 101

- Start compiler with `swipl`
- Stop compiler with `halt.`
- Load files with `[filename].` Put filename in apostrophes if it doesn't work
    - Notice the period at the end.
- Use `;` to show next result when calculating
- Refer to [this page](https://www.swi-prolog.org/pldoc/man?section=debugger) for debugging.
    - trace./notrace.
    - debug/nodebug.
- Always start with `:- use_module(library(clpfd)).` which will give you the ability to use funcitons with **#** a.k.a. *New arithmetics*
- You should almost always use the *New arithmetics*. If you replace everything in a program using the old arithmetics, you will again have a correct program.
- Know the difference between *Traditional arithmetics* and *New arithmetics*
    - `X > 5.` - will try to resolve X and will do nothing.
    - `X #> 5` - will create a definition for X which is greater than 5.
- `=<` and `>=`
- `3 =:= 1+2.` - this calculates both sides and checks whether they are equal.
    - `A =:= 1+2.` would be a prolem (it would try to evaluate A), unless there is an `A is 1+2.` statement somewhere earlier in the program.
        - Again all variables on the right side of `is` must be evaluatable 
- If you do not have arithmetic expressions in the arguments on the **left** then you are ok to have artihmetic expression in the arguments on the **right** of your program.
    - e. g. `len([], N) :- N #= 0.` and `len([_|X], Y) :- len(X, Y - 1).`.
    - However it is **always** safer to not use arithmetic expression in the arguemnts. The second part of upper example would then be `len([_|X], Y) :- Z #= Y -1, len(X, Z).`
    - **Constants (e.g. `0`) are alwso arithmetic expressions!**
- `X = {N: p(N)}` on prolog would be `findall(N, p(N), X).` This can be done **only if** p generates fully evaluateable values for N. (N cannot be in some range)
    - If p has some other varaibles `p(N, K)` which are not still evaluated, then K is a local variable (exits K). If we have a local variable, then it will be used in that p, and not another one that we maybe want to define.
- Universal quantifier `forall(p(X), q(X)).`
    - `forall(p(X), q(X)).` <=> `not(p(X), not(q(X)))`
    - You can use this predicate **only if**:
        - p(X) gives result directly (p cannot set conditions for X)
        - If q has some other varaibles `q(N, K)` which are not still evaluated, then K is a local variable (exits K). If we have a local variable, then it will be used in that p, and not another one that we maybe want to define. 
- label([K]) -> find all Ks and generate them. This is needed here bc otherwise K would not be defined and consequently cannot be used in forall 
- Negation: `\+` or `not` - `not(p(x1, ..., xn))`works only if all of x1, ... ,xn are known
- If you want to get all pairs of elemnts in a list `X = [1, 2, 3, 4], findall([A, B], (append(_, [A|Y], X), member(B, Y)), Z).`
- Useful functions 
    - forall/2
    - findall/3
    - member/2
    - 
        ```
        sublist([], _).
        sublist([A|X], [A|Y]) :- sublist(X, Y).
        sublist(X, [_|Y]) :- sublist(X, Y).
        ```
    - 
- `Nums ins 0..10` gives 
- Opening a file named **file.ext** can be done like this: `["file.ext"].`