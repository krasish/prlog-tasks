
:- use_module(library(clpfd)).

el(A, [A|_]).
el(A, [_|X]) :- el(A, X).

concat([], Y, Y).
concat([A|X], Y, [A|Z]) :- concat(X, Y, Z).

len([], N) :- N #= 0.
len([_|X], Y) :- Y #> 0, len(X, Y - 1).

% Find all even numbers in X
%
% X = [1, 3, 4, 5, 8], findall(N, (member(N, X), N #= 2*K), Y).

% Is it true that all emenets of X are elements of Y
%
% X = [1, 3, 4, 5], Y = [1, 3, 4, 5, 6], forall(member(A, X), member(A, Y)).

% Is it true that all emenets of X are even
%
% X = [1, 3, 4, 5], forall(member(A, X), A #= 2 * K).

% check all numers up to sqrt(N)
% label([K]) -> find all Ks and generate them. This is needed here bc otherwise K would not be defined and consequently cannot be used in forall
is_prime(N) :- 
    N #>= 2,
    forall(
        (K #>= 2, K * K #=< N, label([K])),
        (N mod K #\= 0)).

% is_prime2(N) :- 
%     N #>= 2,
%     K #>= 2, 
%     K * K #=< N,
%     not((N mod K #= 0)).

% between(1, 3, N) gives you numbers 1 to 3
% between(1, 3, N) <=> N #>= 1, N #=< 3, label([N])


% Both caluses (2, 3) we have at least one argument which has descending length, so recurrsion has an edn
sublist([], _).
sublist([A|X], [A|Y]) :- sublist(X, Y).
sublist(X, [_|Y]) :- sublist(X, Y).


% По даден списък L от списъци, проверява дали за всеки два елемента от L съществува трети елемент съдържащ обяите елементи на другите два
p20160615a(L) :- 
    forall(
        sublist([X,Y], L), %Vseki podspisuk, koito e 2ka <-> vsqka 2ka ot L
        (member(Z, L),  % Взимаме всички други списъци
        forall((member(A, X), member(A, Y)), member(A, Z)))).
% Долния предикат само проверява дали дадено число е делител, но не може да генерира.
% Причината е, че ако не сме дали конкретна стойност за N (т.е. искаме да го генерираме), от условието на forall 
% всички аргуементи да са известни, пролог ще го разгледа като "съществува N" и няма да ни върне конкретните стойност.
% --
%% commonDivisor(N, X) :- forall(member(A, X), N * _ #= A).

% правилно
% --
%%    commonDivisor(N, X) :- 
%%    member(B, X), N * _ #= B, label([N]), % N е делител на някакъв елемент на Х и казаваме (с лейъбл) да използваме знанията, които имаме за N 
%%    forall(member(A, X), N * _ #= A).

% добавена ефективност
% --
commonDivisor(N, X) :- 
    once(member(B, X)), N * _ #= B, label([N]), % понеже на долния ред сме казали, че ще е делител на всичките, не е нужно да итерираме по делителите на всеки един от членовете. Tова прави once.
    forall(member(A, X), N * _ #= A).


gcd(N, X) :- commonDivisor(N, X), forall(commonDivisor(M, X), M #=< N).

p20160903a(L, N) :- 
    sublist(A, L), length(A, N), 
    gcd(GCD_A, A),
    forall(
        (sublist(B, L), T #= N - 1, length(B, T)),
        (GCD_B #\= GCD_A, gcd(GCD_B, B))).


% Сумата на всеки два елемента на Х е елемент на У, когато У поглъща Х
eats(Y, X) :- forall((sublist([A, B], X)), (S #= A + B, member(S, Y))).

% Проверщва дали всеки елемент се поглъща от всеки следващ на него
each_next_eats(X) :- forall((append(_, [A|L], X), member(Next, L)), eats(Next, A)).

perm([], []).
perm([A1|L1], P) :- perm(L1, Pp), concat(X, Y, Pp), concat(X, [A1|Y], P).

% Generira v S redica ot ralzichni elementin na L, v koqto element se poglushta ot sledvashtite elementi na S

% subset_diff(X, Y, Z) - Z е списък от различни елементи на ралзиката X\Y 
subset_diff(X, Y, Z)
