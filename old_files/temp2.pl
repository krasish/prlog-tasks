:- use_module(library(clpfd)).

nat(0).
nat(X) :- X1 #= X - 1, nat(X1).

insert(X, L, RL):- append(A, B, L), append(A, [X|B], RL).

sublist(R, L):- append(_, S, L), append(R, _, S).

removeDuplicates([], []).
removeDuplicates([H|T], [H|R]):- removeDuplicates(T, R), not( member(H, R)).
removeDuplicates([H|T], R):- removeDuplicates(T, R), member(H, R).

split2([], [], []).
split2([H|T], [H|L], R):- split2(T, L, R).
split2([H|T], L, [H|R]):- split2(T, L, R).


pairs(A, B):- nat(N), between(0, N, A), B is N - A.

count([], _, 0).
count([H|T], H, N):- count(T, H, M), N is M + 1.
count([H|T], Y, N):- H \= Y, count(T, Y, N).