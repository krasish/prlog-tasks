:- use_module(library(clpfd)).

len([], 0).
len([_|AA], X) :- X #= Y + 1, len(AA, Y).

sum([], 0).
sum([A|AA], X) :- X #= A + Y, sum(AA, Y).

in_range(K, L, K) :- K #=< L.
in_range(K, L, I) :- K #=< L, K1 #= K + 1, in_range(K1, L, I).

is_square(X) :- X #= P * P, in_range(1, X, P).

is_square_list(L) :- sum(L, S), len(L, Len), is_square(S), is_square(Len).

%task 2

new_func([A1, B1, C1, D1], X) :- X #= (A1 * A1 * D1 * D1) + (C1 * C1 * B1 * B1).
new_func_2([_, B1, _, D1], X) :- X #= D1 * D1 * B1 * B1.

substract_rational_boost([A1, B1, C1, D1], [A2, B2, C2, D2], [A3, B3, C3, D3]) :- A3 #= A1 * B2 - A2 * B1, B3 #= B1 * B2, C3 #= C1 * D2 - C2 * D1, D3 #= D1 * D2.

are_far_enough([A1, B1, C1, D1], [A2, B2, C2, D2]) :-  substract_rational_boost([A1, B1, C1, D1], [A2, B2, C2, D2], [A3, B3, C3, D3]), 
    new_func([A3, B3, C3, D3], X), new_func_2([A3, B3, C3, D3], Y), X // Y #> 4.

some_are_not_far_enough(L) :- append(_, [[A1, B1, C1, D1]|LL], L), append(_, [[A2, B2, C2, D2]|_], LL), not(are_far_enough([A1, B1, C1, D1], [A2, B2, C2, D2])).
all_are_far_enough(L) :- not(some_are_not_far_enough(L)).


% ebava si maikata

subset2([], []).
subset2([_|AA], B) :- subset2(AA, B).
subset2([A|AA], [A|B]) :- subset2(AA, B).

there_is_bigger(S, M) :- subset2(S, X), all_are_far_enough(X), len(X, XL), len(M, ML), ML #< XL.
is_biggest(S, M) :- not(there_is_bigger(S, M)).


max_independent(S, M) :- subset2(S, M), all_are_far_enough(M), is_biggest(S, M). 