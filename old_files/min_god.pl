:- use_module(library(clpfd)).

is_path([[_,_]]).
is_path([[_, B], [B, C]| Tail]) :- is_path([[B, C]| Tail]).

append2([], X, X).
append2([H|Tail], X, [H|Res]) :- append2(Tail, X, Res).

member2(X, [X|_]).
member2(X, [_|Tail]) :- member2(X, Tail).

subset2([], []).
subset2([H|T], [H|L]) :- subset2(T, L).
subset2([_|T], L) :- subset2(T, L).

insert(X, L, Res) :- append2(L1, L2, L), append2(L1, [X|L2], Res).

len2([_], 1).
len2([_|Tail], N) :- N #= N1 + 1,len2(Tail, N1).

permute2([], []).
permute2([A|T], P) :- permute2(T, Pp), insert(A, Pp, P).

biggest_path(E, Bp) :- subset2(E, S), permute2(S, Bp), is_path(Bp), len2(Bp, BpLen), not( ( subset2(E, Ss), permute2(Ss, Bpp), is_path(Bpp), len2(Bpp, BppLen), BppLen #> BpLen ) ).
height(E, H) :- biggest_path(E, P), len2(P, H).


% task 2

% skacha i propada max 3 eidni ci

depth([], 0).
depth([S], N) :- depth(S, N1), N#=N1+1.

can_go([X, Y|Tail]) :- depth(X, XD), depth(Y, YD), XD - YD #< 3, XD - YD #> -3, can_go([Y|Tail]).

gen_paths(L, P) :- permute2(L, P), can_go(P).