customer(tom, smith, 20.55).
customer(alice, smith, 120.55).

get_balance(X, Y) :-
    customer(X, Y, Balance),
    format('~w ~w has ~2f in the bank', [X, Y, Balance]).