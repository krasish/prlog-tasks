:- use_module(library(clpfd)).

fib(0, 0, 1).
fib(N, Z, Next) :- N #> 0, N #= X + 1, Next #= Y + Z, fib(X, Y, Z).

fact(0, 1).
fact(N, Z) :- N #= X + 1, Z#= N * Y, fact(X, Y).

mem2(X, [X|_]).
mem2(X, [_|L]) :- mem2(X, L).

xcat([], Y, Y).
xcat([H|X], Y, [H|Z]) :- xcat(X, Y, Z).

xsubset([], []).
xsubset([A|X], [A|Y]) :- xsubset(X, Y).
xsubset([_|X], Y) :- xsubset(X, Y).

xperm([], []).
xperm([A|X], [A|Y]) :- xperm(X, Y) 