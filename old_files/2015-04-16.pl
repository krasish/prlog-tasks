:- use_module(library(clpfd)).

count_occuences(_, [], 0).
count_occuences(X, [X|T], G) :- G #= G1 + 1, count_occuences(X, T, G1).
count_occuences(X, [Y|T], G) :- X \= Y, count_occuences(X, T, G).

exists_more_occurent(X, L) :- member(Y, L), Y \= X, count_occuences(Y, L, O1), count_occuences(X, L, O2), O1 #> O2.  

most_common(X, L) :- member(X, L), not(exists_more_occurent(X, L)), !.

most_common_count(L, C) :- most_common(X, L), count_occuences(X, L, C).


subset2([], []).
subset2([H|T], [H|G]) :- subset2(T, G).
subset2([_|T], G) :- subset2(T, G).


p(X, S) :- subset2(X, S), most_common_count(S, MostCommCount), not(member(MostCommCount, X)).

el_function(L, R) :- el_function(L, [], R), !.
el_function(L, T, R) :- p(L, X), not(member(X,T)), el_function(L, [X|T], R).
el_function(_, R, R) :- !.