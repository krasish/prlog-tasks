:- use_module(library(clpfd)).

nat(N) :- N #>= 0 ; nat(N - 1).

% lists(X) :- nat(K),
%             length(X, K),
%             forall((member(A, X)), (A #>= 0, A #=< 99)).
% lists(X) :- forall((member(A, X)), (A #>= 0, A #=< 99)).
lists([]).
lists([A|L]) :- A #>= 0, A #=< 99, g([A|L]), lists(L).
