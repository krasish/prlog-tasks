:- use_module(library(clpfd)).

nat(c).
nat(s(X)) :- nat(X).

sum1(X, c, X).
sum1(X, s(Y), s(Z)) :- sum1(X, Y, Z).

gcd(0, N, N, 0, 1).
gcd(M, N, D, U, V) :- M #> 0, N #>= M, N1 #= N - M, U #= U1 - V, gcd(M, N1, D, U1, V).
gcd(M, N, D, U, V) :- M #> N, gcd(N, M, D, V, U).

fib(0, 0).
fib(1, 1).
fib(N, X) :- N #>= 2, A #= N - 1, B #= N - 2, fib(A, F1), fib(B, F2), X #= F1 + F2.

fib2(0, 0, 1). 
fib2(N, Prev, Curr) :- N #>0, N1 #= N - 1, Prev2 #= Curr - Prev, fib2(N1, Prev2 ,Prev).

fact(0, 1).
fact(N, F) :- N #> 0, F #> 0, N1 #= N - 1, fact(N1, Prev), F #= Prev * N.

mem2(X, [X|_]).
mem2(X, [_|T]) :- mem2(X, T).

cat2([], B, B).
cat2([X|A1], B, [X|C1]) :-  cat2(A1, B, C1).

subset([], []).
subset([A|L], [A|S]) :- subset(L, S).
subset([_|L], S) :- subset(L, S).

% Zad1 za domashno - Da ne se izkarvat podmnojestva, koito imat povtarqshti se elementi
subset2([], []).
subset2([A|L], [A|S]) :- subset2(L, S), \+ mem2(A, S).
subset2([B|L], S) :- subset2(L, S), \+ mem2(B, S).

perm([], []).
perm([A1|L1], P) :- perm(L1, Pp), cat2(X, Y, Pp), cat2(X, [A1|Y], P).

% Zad2 za domashno - Da ne se izkarvat edni i suhsti permutacii za kogato ima povtarqshti se elementi
% Ideq - funkciq koqto namira dublikatite, posle funkciq koqto dava spisuka bez dublikati...

is_sorted([]).
is_sorted([_]).
is_sorted([A, B|S]) :- A #=< B,  is_sorted([B|S]).

% Ako ima ravni elementi shte se generirat 2 rezultata, koito shte sa viidimo ednakvi zashtoto shte ima 2 permutacii, koito sa sortirani
sort2([], []).
sort2([A], [A]).
sort2(L1, [A1|S1]) :- perm(L1, [A1|S1]), is_sorted([A1|S1]).

merge([],Y, Y).
merge([X1|X],[], [X1|X]). % ako e samo X shte generirame dva puti ([],[])
merge([A1|L1], [A2|L2], [A1|Z]) :- A1 #=< A2, merge(L1, [A2|L2], Z).
merge([A1|L1], [A2|L2], [A2|Z]) :- A2 #< A1, merge(L2, [A1|L1], Z).

split([], [], []).
split([A], [A], []).
split([A, B|L], [A|L1], [B|L2]) :- split(L, L1, L2).

merge_sort([], []).
merge_sort([A], [A]).
merge_sort([A, B|L], S) :- split([A, B|L], X, Y), merge_sort(X, K),merge_sort(Y, M), merge(K, M, S).

%HW: Umnojenie na dva polinoma prestaveni kato spisuci
sum_pol([], X, X).
sum_pol([Y1|Y], [], [Y1|Y]).
sum_pol([X1|X],[Y1|Y], [Z1|Z]) :- Z1 #= X1 + Y1, sum_pol(X, Y, Z).

mult_const(_, [], []).
mult_const(A, [F0|F], [G0|G]) :- G0 #= A * F0, mult_const(A, F, G).

merge_pol(L, [], L).
merge_pol([], [A1|A], [A1|A]).
merge_pol([A1|A], [B1|B], [A1, B1|L]) :- merge_pol(A, B, L).

mult_pol([F0], G, H) :- mult_const(F0, G, H).
mult_pol([F0, F1|F], [G0], H) :- mult_const(G0, [F0, F1|F], H).
mult_pol([F0, F1|F], [G0, G1|G], H) :- 
    split([F0, F1|F], Feven, Fodd),
    split([G0, G1|G], Geven, Godd),
    sum_pol(Feven, Fodd, Fsum),
    sum_pol(Geven, Godd, Gsum),
    mult_pol(Fsum, Gsum, Hsum),
    mult_pol(Feven, Geven, Heven1),
    mult_pol(Fodd, Godd, Heven2),
    mult_const(-1, Heven1, Hneg1),
    mult_const(-1, Heven2, Hneg2),
    sum_pol(Hsum, Hneg1, Hoddtemp),
    sum_pol(Hoddtemp, Hneg2, Hodd),
    sum_pol(Heven1, [0|Heven2], Heven), % otmestvaneto 
    merge_pol(Heven, Hodd, H).

len2([], 0).
len2([_|L], N) :- N #= 1 + N1, len2(L, N1).


% Grafut se predstavq (G, E), kudeto G sa vertexite, a E e mnojestvo ot dvoiki i kogato (u, v) prinadleji na  E  -> ima rebro ot u do v
% Ideq  1. Generirame perm n V - P
%       2. Proverqvame dali P e put
% [v1, v2, ..., vn] e put <=> za Vsqko i < n ((vi, vi+1) prinadleji na E)
%                                         |
%                                         \/
%                            ne sushtestvuva i < n , za koito !((vi, vi+1) prinadleni na E)

is_edge(U, V, E) :- mem2([U, V], E).
is_edge(U, V, E) :- mem2([V, U], E).

% P e put - ima posledovatelni U, V в P, които не образуват ребро 
is_not_path(P, E) :- cat2(_, [U, V| _], P), not(is_edge(U, V, E)).
is_path(P, E) :- not(is_not_path(P, E)).

%Graph = [1, 2, 3, 4, 5].
%Edges = [[1, 2], [2, 3], [1, 3], [3, 4], [4, 5], [5, 1]].

is_hamiltonian([V, E], P) :- perm(P, V), is_path(E, P).


% Дадено: G = (V, E)
% Търси се: 1.clique(G, C)
% генерира в С всички клики на G
%
% max_clique(G, C)
% генерира в C всички 
%   - максимални по включване клики на G
%   - максимални по големина клики на G 
%
% Искаме да използваме 
%   - генератори на пермутации; подмножества/подредици
%   - разпознаватели
% 
% Идея: 1. Генерираме С - подредица на V
%       2. Проверяваме дали С е клика
%
%       С е клика в G <-> за всяко u, v; u != v, принадлежащо на C => (u, v) принадлежи на Е
%                            |                                       
%                            \/
%                          не съществува u, v приналдежащо на С (u, v; u != v и (u, v) Не принадлежи на Е)
%
% is_not_clique(E, C)
% C не е клика
% C = L1 | [U| L2] | [V | L3]
% _U_V_
is_not_clique(E, C) :- cat2(_, [], [])





