:- use_module(library(clpfd)).

remove(X, L, RL):- append(A, [X|B], L), append(A, B, RL).

in_int(El, X, Y) :- member(El, X), member(El, Y).
interseciton(X, Y, I) :- append(_, [P|_], I), in_int(P, X, Y).

contains(L, X) :- not(
    (member(K, X), not(member(K, L))
    )).

help2(Z, L) :- member(T, L), not((contains(Z, T))).
negative(L) :- member(X, L), member(Y, L), interseciton(X, Y, I), help2(I, L).

% p(L) :- member(X, L), member(Y, L), member(Z, L), interseciton(X, Y, I), contains(I, Z).

help3(X, Y, Z) :- not(( member(M, X) , member(M, Y) ,not(member(M, Z)) )).
p(L) :- not( (member(X, L), remove(X,L,LL),member(Y, LL), not( (remove(Y,LL,LLL), member(Z, LLL), help3(X, Y, Z)) ) )).

% p2(L) :- not( (member(X, L), member(Y, L), not( (member(Z, L), member(K, X), member(K, Y), member(K, Z)) ) )).

subset([], []).
subset([H|T], [H|G]) :- subset(T, G).
subset([_|T], G) :- subset(T, G).

gcd(X, 0, X).
gcd(X, Y, G) :- X > Y, C #= X mod Y, gcd(Y, C, G).
gcd(X, Y, G) :- Y > X, gcd(Y, X, G).

pred1(L) :- not( (append(_, [X|LL], L), append(_, [Y|_], LL), gcd(X, Y, G), G \= 1) ).

% el_function([], Stack, Stack).
% el_function([], Stack, Stack).
% el_function(M, [X|L]):-pred2(M, X), el_function(M, L).



pred2(M, X) :- subset(M, X), pred1(X).

el_function(L, R) :- el_function(L, [], R), !.
el_function(L, T, R) :- pred2(L, X), not(member(X,T)), el_function(L, [X|T], R).
el_function(_, R, R) :- !.