:- use_module(library(clpfd)).

is_pythagorean(N, M, K) :- N^2 + M^2 #= K^2.
triple_sum(N, M, K, S) :- N + M + K #= S.

member2(X, [X|_]).
member2(X, [_|Tail]) :- member2(X, Tail).

append2([], X, X).
append2([G|Tail], X, [G|A]) :- append2(Tail, X, A).

get_second_elements([], []).
get_second_elements([[]|Tail], SecEls) :- get_second_elements(Tail, SecEls).
get_second_elements([[_]|Tail], SecEls) :- get_second_elements(Tail, SecEls).
get_second_elements([[_, B|_]|Tail], [B|SecEls]) :- get_second_elements(Tail, SecEls).

max_element([M], M).
max_element([H|Tail], H) :- H #> TailMax, max_element(Tail, TailMax).
max_element([H|Tail], TailMax) :- H #=< TailMax, max_element(Tail, TailMax).

less_than_some(X, L) :- X #< M, max_element(L, M).

last_but_one([X, _], X).
last_but_one([_, B, C|Tail], X) :- last_but_one([B, C|Tail], X).

list_sum([], 0).
list_sum([A|Tail], Sum) :- Sum #= Sum #= A + SSum, list_sum(Tail, SSum).

sum_of_some(N, [X|_]) :- list_sum(X, N).
sum_of_some(N, [_|Tail]) :- sum_of_some(N, Tail).

p(X, N, M, K) :- last_but_one(X, LBO), get_second_elements(X, SecEls),
                 sum_of_some(N, X), not( (member2(Mem, SecEls), member2(N, Mem), member2(K, Mem)) ),
                is_pythagorean(N, M, K), triple_sum(N, M, K, Sum), less_than_some(Sum, LBO).