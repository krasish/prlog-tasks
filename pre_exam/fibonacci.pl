fib(0, 1).
fib(X, Y) :- fib(Z, X), Y #= Z + X.


% A0 = 1, A1 = 2, A2 = 3, An = A(n-2) - A(n-3) + 10 * N


p(0, 1).
p(1, 2).
p(2, 3).
p(N, X) :- Arg1 #= N - 2, Arg2 #= N - 3, p(Arg1, S), p(Arg2, SS), X #= S - SS  + 10 * N.

p2(2, 1, 2, 3).
p2(N, X, Y, Z) :- N #= N1 + 1, p2(N1, C, X, Y), Z #= X - C + 10 * N.


split([], [], []).
split([X], [X], []).
split([A, B|Tail],[A|A1], [B|B1] ) :- split(Tail, A1, B1).

nat(0).
nat(X) :- Y #= X - 1, nat(Y).