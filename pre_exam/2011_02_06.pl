:- use_module(library(clpfd)).

subset([], []).
subset([X|Tail], [X|Subseq]) :- subset(Tail, Subseq).
subset([_|Tail], Subseq) :- subset(Tail, Subseq).


get_all_wanted_pairs([A, B], [[A, B]]) :- A #> B.
get_all_wanted_pairs([A, B], []) :- A #=< B.
get_all_wanted_pairs([A, B|Tail], [[A, B]|Wanted]) :- A #> B, get_all_wanted_pairs([B| Tail], Wanted).
get_all_wanted_pairs([A, B|Tail], Wanted) :- A #=< B, get_all_wanted_pairs([B| Tail], Wanted).

exists_c([[_,_]]).
exists_c([[_,B]|L]) :- not( (member([_, C], L), C \= B) ).

get_first_elements([], []).
get_first_elements([[A, _]|Tail], [A|Rest]) :- get_first_elements(Tail, Rest).

is_sorted([]).
is_sorted([_]).
is_sorted([A, B|Tail]) :- A #< B, is_sorted([B|Tail]).

p(L) :- get_all_wanted_pairs(L, Pairs), exists_c(Pairs), get_first_elements(Pairs, First), subset(L, S), is_sorted(S), member(X, First), member(X, S).

